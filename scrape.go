package main

import (
	"fmt"
	"net/http"

	"code.google.com/p/go.net/html"
)

func scrapeCRSFToken(o *Options) (string, error) {

	downloadsURL := fmt.Sprintf("https://bitbucket.org/%s/%s/downloads", o.User, o.Proj)
	req, err := http.NewRequest("GET", downloadsURL, nil)
	if err != nil {
		return "", err
	}

	req.SetBasicAuth(o.User, o.Pass)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	doc, err := html.Parse(resp.Body)
	if err != nil {
		return "", err
	}

	return extractCRSFToken(doc), nil
}

func extractCRSFToken(n *html.Node) string {

	crsftoken := ""

	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "input" {

			token := ""
			sawtoken := false

			for _, a := range n.Attr {
				switch a.Key {
				case "name":
					if a.Val == "csrfmiddlewaretoken" {
						sawtoken = true
					}
				case "value":
					token = a.Val
				}
			}

			if sawtoken {
				crsftoken = token
				return
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(n)

	return crsftoken
}
