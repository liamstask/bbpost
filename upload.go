package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

func upload(o *Options, crsftoken string) error {

	req, err := createMultipartPostReq(o, crsftoken)
	if err != nil {
		return err
	}

	client := &http.Client{}
	// see if we're being redirected to sign in - indicates auth is not correct
	client.CheckRedirect = func(req *http.Request, via []*http.Request) error {
		if strings.HasPrefix(req.URL.Path, "/account/signin") {
			return errors.New("user/pass appear to be incorrect")
		}
		return nil
	}

	res, err := client.Do(req)
	if err != nil {
		return err
	}

	if res.StatusCode != http.StatusOK {
		return errors.New(fmt.Sprintf("%s: %s", res.Request.URL, res.Status))
	}

	return nil
}

func createMultipartPostReq(o *Options, crsftoken string) (*http.Request, error) {
	var b bytes.Buffer
	w := multipart.NewWriter(&b)

	f, err := os.Open(o.Post)
	if err != nil {
		return nil, err
	}

	fw, err := w.CreateFormFile("file", filepath.Base(o.Post))
	if err != nil {
		return nil, err
	}

	// pr, pw := io.Pipe()

	if _, err = io.Copy(fw, f); err != nil {
		return nil, err
	}

	// add any other fields
	params := map[string]string{
		"csrfmiddlewaretoken": crsftoken,
	}

	for k, v := range params {
		if err = w.WriteField(k, v); err != nil {
			return nil, err
		}
	}
	w.Close()

	uploadURL := fmt.Sprintf("https://bitbucket.org/%s/%s/downloads", o.User, o.Proj)
	req, err := http.NewRequest("POST", uploadURL, &b)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", w.FormDataContentType())
	req.SetBasicAuth(o.User, o.Pass)

	return req, nil
}
