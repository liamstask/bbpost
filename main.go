package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
)

var (
	flagUser = flag.String("user", "", "bitbucket username")
	flagPass = flag.String("pass", "", "bitbucket password")
	flagProj = flag.String("proj", "", "bitbucket project name")
	flagPost = flag.String("post", "", "path to the resource to post")
)

func usage() {
	log.Println("usage: bbpost -user username -pass password -proj projectname -post /path/to/resource")
}

type Options struct {
	User, Pass, Proj, Post string
}

// default to flags, and override from env
func getOptions() (*Options, error) {

	flag.Parse()

	opts := &Options{
		User: *flagUser,
		Pass: *flagPass,
		Proj: *flagProj,
		Post: *flagPost,
	}

	// XXX: read from env as well

	for k, v := range map[string]string{"user": opts.User, "pass": opts.Pass, "proj": opts.Proj, "post": opts.Post} {
		if v == "" {
			return opts, errors.New(fmt.Sprintf("bbpost: %s not specified", k))
		}
	}

	return opts, nil
}

func main() {

	opts, err := getOptions()
	if err != nil {
		log.Println(err)
		flag.Usage()
		os.Exit(1)
	}

	csrftoken, err := scrapeCRSFToken(opts)
	if err != nil {
		log.Fatal(err)
	}

	if err = upload(opts, csrftoken); err != nil {
		log.Fatal(err)
	}

	log.Println(fmt.Sprintf("bbpost: uploaded %s to bitbucket.org/%s/%s", opts.Post, opts.User, filepath.Base(opts.Proj)))
}
