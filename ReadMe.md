
# bbpost

(hopefully temporary) workaround for posting download artifacts to bitbucket projects.

This is a bit terrible, but bbpost scrapes the contents of the 'downloads' page for a project on bitbucket, and uploads a given file to be hosted there. As a result, the format of the form upload may change and bbpost may stop working.

With any luck, a proper API will be provided for this, rendering bbpost obsolete. This is being tracked in [bitbucket issue #1315](https://bitbucket.org/site/master/issue/3251/add-custom-file-uploads-to-rest-api-bb).

### install

Requires a Go installation for now. Maybe host binaries if it's worth it?

    $ go get bitbucket.org/liamstask/bbpost

This will install the `bbpost` binary to your `$GOPATH/bin` directory.

### usage

    $ bbpost -user username -pass password -proj projectname -post /path/to/file
